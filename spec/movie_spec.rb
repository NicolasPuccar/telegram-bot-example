require 'spec_helper'
require_relative '../app/movie'

describe Movie do
  it 'movie have a title and awards' do
    movie = described_class.new('title', 'awards')
    expect(movie.title).to eq 'title'
    expect(movie.awards).to eq 'awards'
  end
end
