require 'spec_helper'
require_relative '../app/movie_api'
require 'web_mock'

def stub(movie_title, movie_awards)
  api_omdb_response_body = {
    "Title": movie_title,
    "Awards": movie_awards,
    "Response": 'True'
  }

  stub_request(:get, "https://www.omdbapi.com/?apikey&t=#{movie_title}")
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: api_omdb_response_body.to_json, headers: {})
end

def stub_false(movie_title, error)
  api_omdb_response_body = {
    "Response": 'False',
    "Error": error
  }

  stub_request(:get, "https://www.omdbapi.com/?apikey&t=#{movie_title}")
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: api_omdb_response_body.to_json, headers: {})
end

describe MovieApi do
  it 'should find a movie with the title' do
    stub('Titanic', 'Won 11 Oscars. 126 wins & 83 nominations total')
    movie = described_class.new.find_movie('Titanic', ENV['OMDB_KEY'])
    expect(movie.title).to eq 'Titanic'
    expect(movie.awards).to eq 'Won 11 Oscars. 126 wins & 83 nominations total'
  end

  it 'should find a movie with the title with no nominations' do
    stub('Emoji', 'N/A')
    movie = described_class.new.find_movie('Emoji', ENV['OMDB_KEY'])
    expect(movie.title).to eq 'Emoji'
    expect(movie.awards).to eq 'not available'
  end

  it 'should not find a movie with a title that not exist' do
    stub_false('EstaPeliculaNoExiste', 'does not exist')
    expect do
      described_class.new.find_movie('EstaPeliculaNoExiste',
                                     ENV['OMDB_KEY'])
    end.to raise_error(MovieNotFoundError)
  end
end
