class Movie
  def initialize(movie_title, movie_awards)
    @movie_title = movie_title
    @movie_awards = movie_awards
  end

  def title
    @movie_title
  end

  def awards
    @movie_awards
  end
end
