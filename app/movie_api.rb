require 'byebug'
require 'faraday'
require_relative '../app/movie'
class MovieNotFoundError < StandardError
end

class MovieApi
  include Faraday

  NOT_APPLY = 'N/A'.freeze
  NO_NOMINATIONS_MESSAGE = 'not available'.freeze
  NON_EXISTENT_MOVIE_MESSAGE = 'does not exist'.freeze
  def find_movie(title, api_key)
    # 'byebug'
    # debugger
    api_url = 'https://www.omdbapi.com/'.freeze
    response = Faraday.get(api_url, { t: title, apikey: api_key })
    response_json = JSON.parse(response.body)
    raise MovieNotFoundError, NON_EXISTENT_MOVIE_MESSAGE if response_json['Response'] == 'False'

    if response_json['Awards'] == NOT_APPLY
      Movie.new(response_json['Title'], NO_NOMINATIONS_MESSAGE)
    else
      Movie.new(response_json['Title'], response_json['Awards'])
    end
  end
end
